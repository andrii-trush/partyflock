/**
 * Created by andrii.trush on 26.01.2017.
 */
function is_touch_device() {
    return 'ontouchstart' in window || navigator.maxTouchPoints;
}
GMaps.prototype.addStyles = function (options) {
    var control = this.createControl(options);

    this.controls.push(control);
    this.map.controls[control.position].push(control);

    return control;
};
;(function () {
    "use strict";
    jQuery.fn.headerUpdatesScroll = function () {
        var container = this;
        var browserW = $(window).width();
        var containerW = $(container).parent().width();
        var containerL = $(container).parent().offset().left;
        var diff = browserW - (containerL + containerW);
        console.log(diff);
        if (-30 < diff < 30) {
            $(container).css('right', (Math.abs(diff) - 50) * (-1) + 'px');
        }
        else {
            $(container).css('right', 15 + 'px');
        }
        if (!is_touch_device()) {
            $(container).mCustomScrollbar({
                axis: "x", // horizontal scrollbar
                autoHideScrollbar: true // horizontal scrollbar
            });
        }
        else if ($(container).hasClass('mCustomScrollbar')) {
            $(container).mCustomScrollbar("destroy")
        }
    };
})(jQuery)
;
(function () {
    "use strict";
    jQuery.fn.fileSelected = function (event) {
        console.log(event);
        var filename = this.val();
        if (filename.length > 0) {
            var lastIndex = filename.lastIndexOf("\\");
            if (lastIndex >= 0) {
                filename = filename.substring(lastIndex + 1);

                if (filename.length > 25) {
                    filename = filename.slice(0, (21 - filename.length)) + '...'
                }
            }
            var parent = this.closest('.custom-upload');
            parent.find('.placeholder').hide();
            parent.find('.file-name').text(filename).show()
        }
    };
    jQuery.fn.btnCollapse = function (event) {
        var parent = this.closest('.panel');
        if (this.attr('data-collapse') == 'in') {
            parent.find('.panel-body').slideUp("slow");
            this.attr('data-collapse', 'out')
        }
        else {
            parent.find('.panel-body').slideDown("slow");
            this.attr('data-collapse', 'in')
        }
    };
})(jQuery);

;
(function () {
    "use strict";
    jQuery.fn.popupContainer = function () {
        if (jQuery('.back-container').length === 0) {
            this.before('<div class="back-container"></div>');
        }
        var options = $.extend({
            onShow: function () {
            },
            onHide: function () {
            }
        }, arguments[0] || {});
        if (this.attr('data-visible') === 'false') {
            jQuery('.back-container').show();
            jQuery('body').addClass('fixed-body');
            this.attr('data-visible', 'true');
            options.onShow.call(this);
        }
        else {
            jQuery('.back-container').hide();
            this.attr('data-visible', 'false');
            jQuery('body').removeClass('fixed-body');
            options.onHide.call(this);
        }

        jQuery('.back-container, .popup-container .btn-close').on('click', function () {
            jQuery('.back-container').hide();
            jQuery('[data-container]').attr('data-visible', 'false');
            jQuery('body').removeClass('fixed-body');
        })
    };
})(jQuery);
var floatingAddBtn = function () {
    $(window).scroll(function () {

        var headerHeight = $('header').height();

        if ($(window).scrollTop() >= headerHeight) {
            $(".btn-add-new").css({
                position: 'fixed',
                right: 0,
                top: '100px'
            });
        }
        else {
            $(".btn-add-new").css({
                position: 'absolute',
                right: '0',
                top: '100px'
            });
        }
    })
};
$(document).ready(function () {
    $('.anchor-link').on('click', function (e) {
        e.preventDefault();
        var container = $(this).attr('data-target');
        $('html, body').animate({scrollTop: $('[data-block="' + container + '"]').offset().top - 100}, 'slow');
    });
    $('.container-slider').each(function () {
        if (!$(this).hasClass('slick-initialized')) {
            $(this).slick({
                dots: false,
                slidesToShow: ($(this).attr('data-slides-sm').length > 0) ? $(this).attr('data-slides-sm') : 4,
                slidesToScroll: 1,
                prevArrow: '<button type="button" class="btn btn-white btn-parallel btn-left btn-lg"><i class="fa fa-angle-left fa-2x"></i></button>',
                nextArrow: '<button type="button" class="btn btn-white btn-parallel btn-right btn-lg"><i class="fa fa-angle-right fa-2x"></i></button>',
                responsive: [
                    {
                        breakpoint: 991,
                        settings: {
                            arrows: true,
                            slidesToShow: 2,
                            centerMode: true,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 570,
                        settings: {
                            arrows: false,
                            slidesToShow: 1,
                            centerMode: true,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 359,
                        settings: {
                            arrows: false,
                            slidesToShow: 1,
                            centerMode: false,
                            slidesToScroll: 1
                        }
                    }

                ]
            })
        }
    });

    if (is_touch_device()) {
        $('.dropdown[data-dropdown]>a').on('click', function (event) {
            event.preventDefault();
            if ($(this).parent().hasClass("open")) {
                $(this).parent().removeClass("open");
                $(this).find("i.fa.fa-angle-down").addClass('fa-angle-up')
            }
            else {
                $('.dropdown[data-dropdown]').removeClass('open');
                $(this).parent().addClass("open");
                $(this).find("i.fa.fa-angle-down").removeClass('fa-angle-up');
                if ($(this).parent().attr('data-dropdown') == 'location') {
                    event_map_1();
                }
                $(this).parent().find('.FillImage').fillImage()
            }
        });
    } else {
        $('.dropdown[data-dropdown]').hover(
            function (event) {
                event.preventDefault();
                if ($(this).hasClass("open")) {
                    $(this).removeClass("open");
                    $(this).find(">a>i.fa.fa-angle-up").removeClass('fa-angle-up').addClass('fa-angle-down');
                }
                else {
                    $('.dropdown[data-dropdown]').removeClass('open');
                    $(this).find(">a>i.fa.fa-angle-down").removeClass('fa-angle-down').addClass('fa-angle-up');
                    $(this).addClass("open");
                    if ($(this).attr('data-dropdown') == 'location') {
                        event_map_1();
                    }
                    $(this).parent().find('.FillImage').fillImage()
                }
            }
        );
    }

    $('.img-container img, .img-absolute').fillImage();
    $('body').on('click', function (e) {
        if (!$('.dropdown[data-dropdown]').is(e.target) && $('.dropdown[data-dropdown]').has(e.target).length === 0 && $('.open').has(e.target).length === 0) {
            $('.dropdown[data-dropdown]').removeClass('open');
        }
    });

    $('#mobile-menu').on('show.bs.collapse', function () {
        $('body').css('overflow', 'hidden');
    }).on('hidden.bs.collapse', function () {
        $('body').removeAttr('style');
    });
    $('.container-account').on('show.bs.collapse', function () {
        $('.collapse-container').show();
        $('.collapse-container').on('click', function () {
            $('.collapse').collapse('hide')
        });
        $(this).find('.btn-close').on('click', function () {
            $('.collapse').collapse('hide')
        })
    }).on('hidden.bs.collapse', function () {
        $('.collapse-container').hide()
    });
    $('.btn-collapse').on('click', function () {
        $(this).btnCollapse()
    });
});

$(window).on('orientationchange resize load', function () {
    if ($('.btn-add-new').length > 0) {
        floatingAddBtn();
    }
    $('[data-page="account"]').accountFooter();
    if ($('[data-page="home-live"]').length > 0) {
        $('[data-page="home-live"]').find('header .container-absolute').headerUpdatesScroll(is_touch_device());
    }
    if ($('[data-scroll="custom"]').length > 0) {
        if (!is_touch_device()) {
            $('[data-scroll="custom"]').mCustomScrollbar({
                axis: 'x'
            });
        }
        else if ($('[data-scroll="custom"]').hasClass('mCustomScrollbar')) {
            $('[data-scroll="custom"]').mCustomScrollbar("destroy")
        }
    }
    $('.img-container img, .img-absolute').fillImage();

    $('[data-container="flyer"]').popupContainer({
        onShow: function () {
            var $container = $(this).find('.flyers');
            if ($container.hasClass('slick-initialized')) {
                $container.slick({settings: "unslick"});
            }
            $container.slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
                speed: 500,
                fade: true,
                prevArrow: '<button type="button" class="arrow btn btn-prev"><i class="fa fa-angle-left"></i></button>',
                nextArrow: '<button type="button" class="arrow btn btn-next"><i class="fa fa-angle-right"></i></button>',
                cssEase: 'linear',
                adaptiveHeight: true
            });
        }
    })

});

(function ($) {
    $.fn.accountFooter = function () {
        var footerHeight = this.find('footer').outerHeight();
        return this.css('padding-bottom', footerHeight + 'px')
    };
})(jQuery);

var headerHomeReviewSlick = function () {
    var $slick_slider = $('.header-sliders'),
        settings = {
            dots: true,
            slidesToShow: 3,
            arrows: false,
            slidesToScroll: 3,
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        arrows: false,
                        slidesToShow: 2,
                        centerMode: true,
                        slidesToScroll: 1
                    }
                }, {
                    breakpoint: 600,
                    settings: "unslick"
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        };
    $slick_slider.slick(settings);
    $slick_slider.find('.img-absolute').fillImage();

    // reslick only if it's not slick()
    $(window).on('resize', function () {
        $slick_slider.find('.img-absolute').fillImage();
        if ($(window).width() < 600) {
            if ($slick_slider.hasClass('slick-initialized')) {
                $slick_slider.slick('unslick');
            }
            return
        }

        if (!$slick_slider.hasClass('slick-initialized')) {
            return $slick_slider.slick(settings);
        }
    });
};

