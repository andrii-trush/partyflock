/**
 * Created by andrii.trush on 24.01.2017.
 */

'use strict';

var gulp = require('gulp'),
    watch = require('gulp-watch'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    stripComments = require('gulp-strip-comments'),
    concat = require("gulp-concat"),
    autoprefixer = require('gulp-autoprefixer'),
    minify = require('gulp-minify');


var path = {
    build: {
        js: 'assets/js/',
        fonts_inside: 'assets/css/fonts/',
        fonts: 'assets/fonts/',
        css: 'assets/css/',
        scss: 'src/scss/',
        img: 'assets/images/'

    },
    src: {
        js: [
            'node_modules/jquery/dist/jquery.js',
            'node_modules/gmaps/gmaps.js',
            'node_modules/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.js',
            'node_modules/bootstrap-sass/assets/javascripts/bootstrap.js',
            'node_modules/slick-carousel/slick/slick.js',
            'node_modules/jquery-validation/dist/jquery.validate.js',
            'node_modules/jquery-validation/src/localization/messages_nl.js',
            'node_modules/bootstrap-select/js/bootstrap-select.js',
            'node_modules/fill-image/jquery.fill-image.js',
            'node_modules/jquery-validation/dist/additional-methods.js',
            'bower_components/bootstrap-tabs-x/js/bootstrap-tabs-x.js',
            'src/js/*.js'
        ],
        fonts: [
            'node_modules/font-awesome/fonts/*.*',
            'src/fonts/*.*'
        ],
        fonts_inside: [
            'node_modules/slick-carousel/slick/fonts/*.*'
        ],
        sass: [
            'src/scss/*.scss',
            'src/scss/*/*/*.scss',
            'src/scss/*/*.scss'
        ]
    },
    watch: {
        js: 'src/js/*.js',
        sass: 'src/scss/**/*.scss'
    }
};

gulp.task('fonts:build', function () {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
});
gulp.task('fonts_inside:build', function () {
    gulp.src(path.src.fonts_inside)
        .pipe(gulp.dest(path.build.fonts_inside))
});
gulp.task('sass:build', function () {
    gulp.src(path.src.sass)
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(sourcemaps.write({includeContent: false}))
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], {cascade: true}))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(path.build.css));
});

gulp.task('js:build', function () {
    gulp.src(path.src.js)
        .pipe(concat('partyflock.js'))
        .pipe(stripComments())
        .pipe(minify({
            ext: {
                src: '.js',
                min: '.min.js'
            }
        }))
        .pipe(gulp.dest(path.build.js));
});

gulp.task('build', [
    'js:build',
    'fonts:build',
    'fonts_inside:build',
    'sass:build'
]);
gulp.task('watch', function () {
    watch([path.watch.js], function (event, cb) {
        gulp.start('js:build');
    });

    watch([path.watch.sass], function (event, cb) {
        gulp.start('sass:build');
    })
});
gulp.task('default', [
    'build',
    'watch'
]);